package ir.parsmehan.childapp.Utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by samir on 7/23/17.
 */

public class MApp extends Application {
    protected static Context context = null;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}
